FROM openjdk:8-jre-alpine
ADD /src/main/resources/application.properties //

COPY /build/libs/*-1.0-SNAPSHOT.jar demo-1.0-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "/demo-1.0-SNAPSHOT.jar"]

